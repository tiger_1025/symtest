<?php

namespace App\Form;

use App\Entity\News;
use App\Entity\Tag;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ["attr"=>["placeholder"=>"Введите заголовок"]])
            ->add('body', TextareaType::class, ["attr"=>["placeholder"=>"Введите содержимое новости"]])
	        ->add("user", TextType::class, ["attr"=>["placeholder"=>"Имя пользователя"]])
	        ->add("tags", CollectionType::class,
		        [
		        	"entry_type"=>TagFormType::class,
			        "entry_options" => array("label" => false),
			        "allow_add"=>true,
			        "allow_delete"=>true,
			        "prototype"=>true
		        ]
	        )
            ->add('poster', FileType::class, ["mapped"=>false])
            ->add('save', SubmitType::class);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
