<?php

namespace App\Controller;

use App\Form\NewsFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\DBAL\Driver\Connection;

use App\Entity\News;
use App\Entity\NewsFormData;
use App\Entity\Tag;

class NewsController extends AbstractController
{

	public function add()
	{
		/*
		$em = $this->getDoctrine()->getManager();

		$news = new News();
		$news->setTitle("test title ".mt_rand(0,100));
		$news->setBody("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus feugiat ipsum quam, nec iaculis ante vulputate et. Integer quis justo malesuada, blandit magna dignissim, pretium nulla. Aenean tincidunt convallis est, nec mollis elit ornare pulvinar.");
		$news->setPoster("/img/poster.jpg");
		$news->setUser("anon.");

		$em->persist($news);
		$em->flush();

		$rand_tags = ["#lorem#","#ipsum#","#dolor#","#sit amet#"];

		$tag = new Tag();
		$tag->setValue($rand_tags[mt_rand(0,count($rand_tags)-1)]);
		$tag->setNewsId($news->getId());
		$em->persist($tag);
		$em->flush();
		*/
		return new Response("ok");
	}

	public function page_main($tag = "")
	{
		$limit = 4;
		$page = 0;

		// Грузим количество новостей для ограничения страниц
		if (isset($_GET["page"])) $page = intval($_GET["page"])-1; if ($page<0) $page=0;
		if ($tag=="") $news_count = $this->getDoctrine()->getRepository(News::class)->getCountAll();
		else $news_count = $this->getDoctrine()->getRepository(News::class)->getCountTag($tag);

		// Страничная магия
		$pages = intdiv($news_count,$limit); if ($pages*$limit<$news_count) $pages++;
		$page_prev = ($page<0?0:$page-1)+1;
		$page_next = ($page==$pages-1?0:$page+2);
		$offset = $page * $limit;

		// Грузим сами новости
		if ($tag=="") $list = $this->getDoctrine()->getRepository(News::class)->findBy([],["created"=>"DESC","id"=>"DESC"],$limit,$offset);
		else $list = $this->getDoctrine()->getRepository(News::class)->searchTag($limit,$offset,$tag);

		return $this->render("news/news_list.html.twig",array(
			"list"=>$list,
			"page"=>$page+1,
			"pages"=>$pages,
			"page_next"=>$page_next,
			"page_prev"=>$page_prev,
			"tag"=>$tag
		));
	}

	//ValidatorInterface $validator,
	public function page_create(Request $request)
	{
		$news = new News();
		$form = $this->createForm(NewsFormType::class, $news,
			[
				"action" => $this->generateUrl("news_page_create"),
				"method" => "POST"
			]);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
			$file = $request->files->get("news_form")["poster"];
			$uploads_directory = $this->getParameter("uploads_directory");
			$filename = md5(uniqid()).".".$file->guessExtension();
			$file->move($uploads_directory, $filename);
			$news->setPoster($filename);

			// Сохранить в базу
			$em = $this->getDoctrine()->getManager();
			$em->persist($news);
			$em->flush();
			$this->redirectToRoute("news_main");
		}

		return $this->render("news/news_create.html.twig",["form"=>$form->createView()]);
	}

}