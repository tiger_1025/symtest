<?php

namespace App\Repository;

use App\Entity\News;
use App\Entity\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\OrderBy;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getCountAll()
	{
		return $this->createQueryBuilder("n")->select("count(n.id)")->getQuery()->getSingleResult()[1];
	}

	public function getCountTag($tag)
	{
		return $this->createQueryBuilder("n")
			->select("count(n.id)")
			->leftJoin("App\Entity\Tag","tg",Join::WITH, "tg.news_id = n.id")
			->andWhere("tg.value LIKE :tagval")
			->setParameter("tagval", $tag)
			->getQuery()
			->getSingleResult()[1];
	}

	public function searchAll($limit, $offset)
	{
		return $this->findBy([],["created"=>"DESC","id"=>"DESC"], $limit, $offset);
	}

	public function searchTag($limit, $offset, $tag)
	{
		return $this->createQueryBuilder("news")
			->leftJoin("App\Entity\Tag","tg",Join::WITH,"tg.news_id = news.id")
			->andWhere("tg.value LIKE :tagval")
			->setParameter("tagval",$tag)
			->orderBy("news.created","DESC")
			->orderBy("news.id","DESC")
			->setFirstResult($offset)
			->setMaxResults($limit)
			->getQuery()
			->getResult();
	}
}
